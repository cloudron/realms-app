FROM cloudron/base:0.8.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io>

# This code is heavily borrowed from https://github.com/scragg0x/realms-wiki/blob/master/docker/Dockerfile

RUN apt-get update && \
    apt-get install -y virtualenv python2.7-dev python-pip libldap2-dev libsasl2-dev libevent-dev python-gevent libmysqlclient-dev python-mysqldb && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /app/code /run/realms-wiki/webassets-cache

RUN pip install virtualenv
RUN virtualenv /app/code/
RUN /app/code/bin/pip install realms-wiki gevent mysql-python redis
# https://github.com/pyca/bcrypt/issues/15#issuecomment-57650938
RUN /app/code/bin/python -c "import bcrypt"

ADD start.sh /app/code/start.sh
ADD realms-wiki.json.template /app/code/realms-wiki.json.template
RUN ln -s /run/realms-wiki/realms-wiki.json /app/code/realms-wiki.json

# https://github.com/miracle2k/webassets/issues/355
RUN ln -s /run/realms-wiki/webassets-cache /app/code/local/lib/python2.7/site-packages/realms/static/.webassets-cache

CMD [ "/app/code/start.sh" ]

