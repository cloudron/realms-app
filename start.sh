#!/bin/bash

set -eu

echo "Creating configuration"
sed -e "s,##APP_ORIGIN,${APP_ORIGIN}," \
    -e "s,##MYSQL_URL,${MYSQL_URL}," \
    -e "s,##SECRET_KEY,$(pwgen -1cns 64)," \
    -e "s,##LDAP_URL,${LDAP_URL}," \
    -e "s,##MYSQL_URL,${MYSQL_URL}," \
    -e "s/##REDIS_HOST/${REDIS_HOST}/" \
    -e "s/##REDIS_PORT/${REDIS_PORT}/" \
    -e "s/##REDIS_PASSWORD/${REDIS_PASSWORD}/" \
    -e "s/##LDAP_BIND_DN/${LDAP_BIND_DN}/" \
    -e "s/##LDAP_BIND_PASSWORD/${LDAP_BIND_PASSWORD}/" \
    -e "s/##LDAP_USERS_BASE_DN/${LDAP_USERS_BASE_DN}/" \
     /app/code/realms-wiki.json.template > /run/realms-wiki/realms-wiki.json

chown -R cloudron:cloudron /app/data /run/realms-wiki

echo "Initializing database"
cd /app/code
/usr/local/bin/gosu cloudron:cloudron /app/code/bin/realms-wiki create_db

echo "Starting unicorn"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/bin/gunicorn \
  --name realms-wiki \
  --access-logfile - \
  --error-logfile - \
  --worker-class gevent \
  --workers 1 \
  --bind 0.0.0.0:5000 \
  --chdir /run/realms-wiki \
  'realms:create_app()'

